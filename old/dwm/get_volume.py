import subprocess
import re

ps = subprocess.Popen("pacmd stat".split(), stdout=subprocess.PIPE)
out = ps.communicate()[0].decode('utf-8')
source = out.split("Default sink name: ")[1].split('\n')[0]

ps = subprocess.Popen("pactl list sinks".split(), stdout=subprocess.PIPE)
out = ps.communicate()[0].decode('utf-8')
volume = out.split(source)[1].split("Volume:")[1].split("/")[1].split('%')[0]
volume = re.sub('[  ]', '', volume)

print(volume)
