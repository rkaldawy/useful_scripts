import os, os.path
import random as rand
import subprocess as sp

context = '/tmp/wallpaperIdx'
with open(context, 'w+') as f:
    try:
        idx = int(f.read())
    except:
        idx = -1

directory = '/home/rkaldawy/Pictures/wallpaper'
wallpaper_count = len([name for name in os.listdir(directory)])

suffix = idx
while suffix == idx:
    suffix = rand.randint(0, wallpaper_count-1)

print suffix

path = directory + '/wallpaper' + str(suffix) + '.jpg'
out = sp.check_output(['feh', '--bg-scale', path])
print out

with open(context, 'w') as f:
    f.write(str(suffix))
