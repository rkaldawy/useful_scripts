import subprocess
import re
import sys
import os


def execute_command(cmd):
    ps = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    return ps.communicate()[0].decode('utf-8')

if len(sys.argv) < 2 or "@blueberry:" not in sys.argv[1]:
    execute_command("urxvt")
    sys.exit()

with open("/tmp/test.txt", "w+") as f:
   f.write(sys.argv[1]) 
path = sys.argv[1].split("@blueberry:")[1]
home_dir = os.environ['HOME'] 
path = path.replace("~", home_dir)

command = "urxvt -cd " + path
execute_command(command)
