#!/bin/bash

export DISPLAY=:1

LOW=10
CRIT_LOW=3

POWER=$(acpi | cut -d ' ' -f4 | cut -d ',' -f1 | cut -d '%' -f1)
CHARGING_STATUS=$(acpi | cut -d ' ' -f3 | cut -d ',' -f1)

echo $POWER

if [ $CHARGING_STATUS == Charging ]
then
  exit 0
elif [ $POWER -gt $LOW ]
then
  exit 0
elif [ $POWER -le $LOW -a $POWER -gt $CRIT_LOW ]
then
  rofi -e "Battery level is low ($POWER%). Please charge now."
  exit 0 # will think of behavior later
elif [ $POWER -le $CRIT_LOW ] 
then
  systemctl suspend
fi
