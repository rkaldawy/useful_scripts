Note:

I got lazy and downloaded a command called regextract from the AUR. It extracts
the first instance of a regex from a stream. If you want to use these scripts,
you'll either need to download regextract yourself (I recommend it) or replace them with
some sed call which I wouldn't know how to do.