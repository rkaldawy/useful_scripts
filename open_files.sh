#!/bin/bash

SUFFIX=$(echo $1 | cut -d '.' -f2)
echo $SUFFIX

if [ $SUFFIX == pdf ];
then 
  zathura "$1"
else
  # might make shell interactive
  urxvt -e sh -c "vim $1" 
fi
