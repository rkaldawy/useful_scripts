#!/bin/sh

PORT=$(xrandr | grep " connected " | awk '{print $1}' | grep "DisplayPort")

while getopts 'ar' flag; do
  case "${flag}" in
    a) /usr/bin/xrandr --output $PORT --mode 1920x1080 --same-as eDP ;;
    r) /usr/bin/xrandr --output $PORT --off
  esac
done
