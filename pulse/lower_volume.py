#! /usr/bin/env python3

import subprocess
import re
import sys
import os

def execute_command(cmd):
    ps = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    return ps.communicate()[0].decode('utf-8')

out = execute_command("pacmd stat")
source = out.split("Default sink name: ")[1].split('\n')[0]

out = execute_command("pactl list sinks");
volume = out.split(source)[1].split("Volume:")[1].split("/")[1].split('%')[0]
volume = int(re.sub('[  ]', '', volume))

try:
    with open("/tmp/volCtrl", "r") as f:
        sys.exit(0)
except:
    pass

if volume >= 5:
    out = execute_command("pactl set-sink-volume @DEFAULT_SINK@ -5%")
    with open("/tmp/volCtrl", "w+") as f:
        f.write(str(volume-5))
elif volume > 0:
    out = execute_command("pactl set-sink-volume @DEFAULT_SINK@ -"+str(volume)+"%")
    with open("/tmp/volCtrl", "w+") as f:
        f.write(str(0))
else:
    sys.exit(0)

while(1):
    out = execute_command("pactl list sinks");
    updated_volume = out.split(source)[1].split("Volume:")[1].split("/")[1].split('%')[0]
    updated_volume = int(re.sub('[  ]', '', updated_volume))

    if updated_volume != volume:
        break

os.remove("/tmp/volCtrl")
